#define _CRT_SECURE_NO_WARNINGS 1
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>


//typedef struct TreeNode {
//	int val;
//	struct TreeNode* left;
//	struct TreeNode* right;
//}TreeNode;
//
//
//
//bool isSameTree(TreeNode* p, TreeNode* q) {
//	if (p == NULL && q == NULL)
//		return true;
//	//经过判断，p、q不可能全为null
//	if (p == NULL || q == NULL) {
//		return false;
//	}
//	if (p->val != q->val) {
//		return false;
//	}
//	return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//} 


typedef struct BTreeNode {
	int val;
	struct TreeNode* left;
	struct TreeNode* right;
}BTreeNode;


//开辟每一个空间
BTreeNode* BuyNode(int x)
{
	BTreeNode* root = (BTreeNode*)malloc(sizeof(BTreeNode));
	if (root == NULL)
	{
		perror("malloc fail");
		return NULL;
	}

	root->val = x;
	root->left = NULL;
	root->right = NULL;

	return root;
}

//讲二叉树链接（创建二叉树）
BTreeNode* CreatBinaryTree()
{
	BTreeNode* root1 = BuyNode(1);
	BTreeNode* root2 = BuyNode(2);
	BTreeNode* root3 = BuyNode(3);
	BTreeNode* root4 = BuyNode(4);
	BTreeNode* root5 = BuyNode(5);
	BTreeNode* root6 = BuyNode(6);
	BTreeNode* root7 = BuyNode(6);


	root1->left  = root2;
	root1->right = root4;
	root2->left  = root3;
	root4->left  = root5;
	root4->right = root6;
	root5->right = root7;

	return root1;
}
//遍历
int TreeNode(struct BTNode* root) {
	return root == NULL ? 0 : TreeNode(root->left) + TreeNode(root->right) + 1;
}

//前序遍历
void prevOrder(struct BTNode* root, int* a, int* pi) {
	if (root->val == NULL) {
		return;
	}
	a[(*pi)++] = root->val;
	prevOrder(root->left,a,i);
	prevOrder(root->right,a,i);
}

int* postOrderTraversal(struct BTNode* root, int* returnSize)
{					// *returnSize：返回数组的同时包括数组本体以及数组元素数量
	*returnSize = TreeNode(root);
	int* a = (int*)malloc(sizeof(int) * (*returnSize));
}

//检测是否为子树
bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) {
	if (root == NULL)
		return false;
	if (root->val == subRoot->val && isSubtree(root, subRoot))
		return true;
	return isSubtree(root->left, subRoot) || isSubtree(root->right,subRoot);
}
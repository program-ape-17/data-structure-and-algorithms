#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#define MAXSIZE 100
typedef char ElemType;

typedef struct
{
	ElemType data[MAXSIZE];
	int length;
} SqList;

void InitList(SqList* SL) {
	//SL = (SqList*)malloc(sizeof(SqList));
	SL->length = 0;
}

int i = 0;
bool ListInsert(SqList* SL, int i, ElemType e)
{
	if (i < 1 || i > SL->length + 1) {
		return false;
	}
	if (SL->length >= MAXSIZE) {
		return false;
	}
	for (int j = SL->length; j >= i; j--) {
		SL->data[j] = SL->data[j - 1];
	}
	SL->data[i - 1] = e;
	SL->length++;
	return true;
}

void DisPList(SqList SL) {
	for (int i = 0; i < SL.length; i++) {
		printf("%c ", SL.data[i]);
	}
	printf("\n");
}

int ListSize(SqList SL) {
	return SL.length;
}

bool EmptyList(SqList SL) {
	return (SL.length == 0);
}

void ShowEmle(SqList SL, int i) {
	printf("%c\n", SL.data[i - 1]);
}

int ShowPlace(SqList SL, ElemType e) {
	for (int j = 0; j < SL.length; j++) {
		if (SL.data[j] == e) {
			return j + 1;
		}
	}
	return 0;
}

bool ListDelete(SqList* SL, int i)
{
	if (i < 1 || i > SL->length) {
		return false;
	}
	//*e = SL->data[i-1];
	for (int j = i; j < SL->length; j++) {
		SL->data[j - 1] = SL->data[j];
	}
	SL->length--;
	return true;
}

void DestroyList(SqList* SL) {
	free(SL);
}

int main() {
	printf("学号：231123020  姓名：叶运达\n");
	SqList SL;
	InitList(&SL);
	printf("依次插入数据：");
	ListInsert(&SL, 1, 'a');
	ListInsert(&SL, 2, 'b');
	ListInsert(&SL, 3, 'c');
	ListInsert(&SL, 4, 'd');
	ListInsert(&SL, 5, 'e');
	printf("\n");
	printf("顺序表：");
	DisPList(SL);
	printf("顺序表的长度：%d\n", ListSize(SL));
	if (EmptyList(SL)) {
		printf("顺序表为空\n");
	}
	else {
		printf("顺序表不为空\n");
	}
	printf("输出顺序表的第3个元素：");
	ShowEmle(SL, 3);
	printf("\n");
	printf("输出顺序表的a元素的位置：%d", ShowPlace(SL, 'a'));
	printf("\n");
	ListInsert(&SL, 4, 'f');
	printf("输出顺序表：");
	DisPList(SL);
	printf("\n");
	ListDelete(&SL, 3);
	printf("输出顺序表：");
	DisPList(SL);
	printf("\n");
	DestroyList(&SL);
}
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void Swap(int* a, int* b)
{
	int tmp = *a;
	*b = *a;
	*a = tmp;
}

void SelectSort(int* a, int n)
{
	int begin = 0, end = n - 1;

	while (begin < end)
	{
		int mini = begin, maxi = begin;
		for (int i = begin + 1; i <= end; ++i)
		{
			if (a[i] > a[maxi])
			{
				maxi = i;
			}

			if (a[i] < a[mini])
			{
				mini = i;
			}
		}

		Swap(&a[begin], &a[mini]);
		if (begin == maxi) 
		{
			maxi = mini;
		}
		Swap(&a[end], &a[maxi]);
		++begin;
		--end;
	}
}

int main()
{
	int a[] = { 9,1,2,5,7,4,6,3 }; 
	SelectSort(a, sizeof(a) / sizeof(int));
	return 0;
}
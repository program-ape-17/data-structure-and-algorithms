#define _CRT_SECURE_NO_WARNINGS 1  
#include <stdio.h>  
#include <stdlib.h>  

typedef int ElemType;

typedef struct LNode {
    ElemType data;
    struct LNode* next;
} LNode, * LinkList;

/* 尾插法创建链表 */
void CreateList_T(LinkList* L, ElemType arr[], int n) {
    *L = (LNode*)malloc(sizeof(LNode)); // 创建头节点  
    (*L)->next = NULL; // 初始化链表为空  
    LNode* r = *L; // 尾指针  

    for (int i = 0; i < n; i++) {
        LNode* p = (LNode*)malloc(sizeof(LNode)); // 创建新节点  
        p->data = arr[i]; // 设置值  
        p->next = NULL; // 新节点的下一个指针指向NULL  
        r->next = p; // 将新节点连接到链表  
        r = p; // 移动尾指针  
    }
}

/* 输出链表 */
void PrintList(LinkList L) {
    LNode* p = L->next;
    while (p) {
        printf("%d ", p->data);
        p = p->next;
    }
    printf("\n");
}

/* 销毁链表 */
void DestroyList(LinkList* L) {
    LNode* p = *L;
    while (p) {
        LNode* q = p;
        p = p->next;
        free(q);
    }
    *L = NULL;
}

/* 按基准划分算法 */
void Partition(LinkList L) {
    if (!L->next || !L->next->next) return; 
    LNode* pivot = L->next; 
    ElemType x = pivot->data; 
    LNode* lessHead = (LNode*)malloc(sizeof(LNode));   
    LNode* greaterHead = (LNode*)malloc(sizeof(LNode)); 
    LNode* lessTail = lessHead; 
    LNode* greaterTail = greaterHead; 
    LNode* curr = pivot->next;  

    while (curr) {
        if (curr->data < x) {
            lessTail->next = curr;  
            lessTail = curr; 
        }
        else { 
            greaterTail->next = curr; 
            greaterTail = curr;   
        }
        curr = curr->next; 
    }
    lessTail->next = pivot; 
    greaterTail->next = NULL; 
    lessHead->next = lessHead->next; 
    pivot->next = greaterHead->next; 
    free(lessHead);
}

int main() {
    printf("学号：231123020 姓名：叶运达\n");
    ElemType arr[10];
    printf("请输入10个整数：");
    for (int i = 0; i < 10; i++) {
        scanf("%d", &arr[i]);
    }
    LinkList L;
    CreateList_T(&L, arr, 10); // 创建链表  
    printf("原始链表：");
    PrintList(L); // 打印原链表  
    Partition(L); // 进行分区  
    printf("划分后的链表：");
    PrintList(L); // 打印划分后的链表  
    DestroyList(&L); // 销毁链表  
    return 0;
}
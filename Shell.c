#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

//void ShellSort(int* a, int n) {
//	int gap = 3;
//	for (int j = 0; i < gap; j++)
//	{
//		for (size_t i = j; i < n - gap; i += gap)
//		{
//			int end = i;
//			int tmp = a[end + gap];
//			while (end >= 0)
//			{
//				if (tmp < a[end])
//				{
//					a[end + gap] = a[end];
//					end -= gap;
//				}
//				else
//				{
//					break;
//				}
//			}
//		}
//	}
//}

//  希尔排序时间复杂度：O（N^1.3）
void ShellSort(int* a, int n) {
	int gap = n;//gap作为每一组之间的间隔数
	while (gap > 1) 
	{
		gap = gap / 3 + 1;
		for (size_t i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
	
}

PrintArray(int* a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
	

int main()
{
	int a[] = { 2,5,3,8,5,9,1,7};
	ShellSort(a, sizeof(a) / sizeof(int));
	//PrintArray(a, sizeof(a) / sizeof(int));
	return 0;
}
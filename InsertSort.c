#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//插入排序
void InsertSort(int* a, int n)
{
	for (int i = 0;i < n - 1; i++)
	{
		int end = i;//end指代下标位置
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > tmp)
			{
				a[end + 1] = a[end];
				--end;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}

int main()
{
	int a[] = { 4,6,3,9,1,7,2,8 };
	InsertSort(a, 8);
	return;
}
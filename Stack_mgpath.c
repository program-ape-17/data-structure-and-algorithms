#define _CRT_SECURE_NO_WARNINGS  
#include <stdio.h>  
#include <stdlib.h>  
#include <assert.h>  
#include <stdbool.h>  

#define MaxSize 50  
#define STDataType Box  

typedef struct Box {
    int i; // 行  
    int j; // 列  
    int di; // 方向  
} Box;

typedef struct {
    Box data[MaxSize];
    int top; // 栈顶索引  
} StType;

// 栈初始化  
void InitStack(StType** pst) {
    assert(pst);
    *pst = (StType*)malloc(sizeof(StType));
    (*pst)->top = -1;
}

// 栈销毁  
void DestroyStack(StType* pst) {
    assert(pst);
    free(pst);
}

// 判断栈是否为空  
bool StackEmpty(StType* pst) {
    return (pst->top == -1);
}

// 进栈  
bool Push(StType* pst, STDataType e) {
    if (pst->top == MaxSize - 1) {
        return false; // 栈满  
    }
    pst->top++;
    pst->data[pst->top] = e;
    return true;
}

// 出栈  
bool Pop(StType* pst, STDataType* e) {
    if (pst->top == -1) {
        return false; // 栈空  
    }
    *e = pst->data[pst->top];
    pst->top--;
    return true;
}

// 取栈顶元素   
bool GetTop(StType* pst, STDataType* e) {
    if (pst->top == -1) {
        return false; // 栈空  
    }
    *e = pst->data[pst->top];
    return true;
}

// 迷宫数组  
int mg[11][11] = {
    {1,1,1,1,1,1,1,1,1,1},
    {1,0,0,1,0,0,0,1,0,1},
    {1,0,0,1,0,0,0,1,0,1},
    {1,0,0,0,0,1,0,0,0,1},
    {1,0,1,1,1,0,0,0,0,1},
    {1,0,0,0,1,0,0,0,0,1},
    {1,0,1,0,0,0,1,0,0,1},
    {1,0,1,1,1,0,1,1,0,1},
    {1,1,0,0,0,0,0,0,0,1},
    {1,1,1,1,1,1,1,1,1,1},
};

// 迷宫路径寻找函数  
bool mgpath(int xi, int yi, int xe, int ye) {
    Box path[MaxSize], e;
    int i, j, di, i1, j1, k;
    bool find;
    StType* st;
    InitStack(&st);
    e.i = xi; e.j = yi; e.di = -1; 
    Push(st, e);
    mg[xi][yi] = -1;   

    while (!StackEmpty(st)) {
        GetTop(st, &e); 
        i = e.i;
        j = e.j;
        di = e.di; 


        if (i == xe && j == ye) {
            printf("一条迷宫路径如下：\n");
            k = 0;
            while (!StackEmpty(st)) {
                Pop(st, &e);   
                path[k++] = e; 
            }  
            for (int m = k - 1; m >= 0; m--) {
                printf("\t(%d, %d)", path[m].i, path[m].j);
                if ((k - m) % 5 == 0) { 
                    printf("\n");
                }
            }
            printf("\n");
            DestroyStack(st);
            return true; 
        }

        find = false; 
        while (di < 4 && !find) {
            di++;
            switch (di) {
            case 0: i1 = i - 1; j1 = j; break;
            case 1: i1 = i; j1 = j + 1; break; 
            case 2: i1 = i + 1; j1 = j; break; 
            case 3: i1 = i; j1 = j - 1; break; 
            } 
            if (mg[i1][j1] == 0) {
                find = true;
            }
        }

        if (find) {
            st->data[st->top].di = di;
            e.i = i1; e.j = j1; e.di = -1; 
            Push(st, e); 
            mg[i1][j1] = -1; 
        }
        else {
            Pop(st, &e); 
            mg[e.i][e.j] = 0; 
        }
    }

    DestroyStack(st);
    return false; 
}

// 主函数  
int main() {
    printf("学号：231123020  姓名：叶运达\n");
    if (!mgpath(1, 1, 8, 8)) {
        printf("该迷宫问题没有解！\n");
    }
    return 0;
}